var VariableFigures = new Object();
var BackgroundData = new Array();
var UserInputs = {
    "Electricity": 4000,
    "Gas": 24000,
    "Oil": 0,
    "Coal": 0,
    "Wood": 0,
    "Water": 21.04,
    
    "Vehicle": 6530,
    "Bus": 2660,
    "Train": 2800,
    "Flight": 1250,
    
    "Food": 1228.93154,
    "ConsumerGoods": 3678.153846,
    "UKShare": 1100/*,
    
    "Activism": 0*/
}

var Solutions = new Object();
var SolutionsAvg = new Object();

var YearValue = [1,4,12];
var TimeValue = [365, 52, 12, 1];

var AreaCalculations = {
    "Electricity": "var ElectricityFormat = [1, BackgroundData[4][2]]; return document.getElementById('ElectricityInput').value * (YearValue[document.getElementById('ElectricityTime').selectedIndex]/ElectricityFormat[document.getElementById('ElectricityFormat').selectedIndex]);",
    "Gas": "var GasFormat = [1,BackgroundData[11][2],BackgroundData[30][2]]; return document.getElementById('GasInput').value * (YearValue[document.getElementById('GasTime').selectedIndex]/GasFormat[document.getElementById('GasFormat').selectedIndex]);",
    "Oil": "var OilFormat = [1,BackgroundData[24][2],BackgroundData[25][2]]; return ((document.getElementById('OilInput').value * (YearValue[document.getElementById('OilTime').selectedIndex]/OilFormat[document.getElementById('OilFormat').selectedIndex]))*BackgroundData[23][2]/1000)*1000;",
    "Coal": "return (document.getElementById('CoalInput').value * BackgroundData[26][2]/1000)*1000;",
    "Wood": "return ((document.getElementById('WoodInput').value * (BackgroundData[336+document.getElementById('WoodFormat').selectedIndex][1+document.getElementById('WoodDiscard').selectedIndex]))/1000)*1000;",
    "Water": "return document.getElementById('WaterInput').value*(parseFloat(BackgroundData[40][2])+parseFloat(BackgroundData[41][2]));",
    
    "Vehicle": "var Taxi = 1;if(document.getElementById('TaxiDriver').value == 'Yes'){Taxi = 2} return ((document.getElementById('VehicleInput').value * TimeValue[document.getElementById('VehicleTime').selectedIndex])*Taxi)/NumberInHousehold;",
    "Bus": "return document.getElementById('BusInput').value*TimeValue[document.getElementById('BusTime').selectedIndex];",
    "Train": "return document.getElementById('TrainInput').value*TimeValue[document.getElementById('TrainTime').selectedIndex];",
    "Flight": "return (document.getElementById('FlightInput').value*BackgroundData[76][2]/1000)*1000;",
    
    "Food": "var FoodTotal = parseFloat(BackgroundData[118+document.getElementById('RedMeat').selectedIndex][2]) + parseFloat(BackgroundData[118+document.getElementById('Fish').selectedIndex][3]) + parseFloat(BackgroundData[118+document.getElementById('Dairy').selectedIndex][4]) + parseFloat(BackgroundData[118][5]);FoodTotal = FoodTotal + parseFloat(BackgroundData[127+document.getElementById('FoodDistance').selectedIndex][2]) + parseFloat(BackgroundData[133+document.getElementById('FoodOrganic').selectedIndex][2]);FoodTotal = FoodTotal + parseFloat(BackgroundData[141+document.getElementById('FoodMicrowave').selectedIndex][2]) + parseFloat(BackgroundData[147+document.getElementById('FoodCompost').selectedIndex][2]);return (parseFloat(BackgroundData[152+document.getElementById('FoodWaste').selectedIndex][2])*(FoodTotal+0.2))*1000;",
    "ConsumerGoods": "var WeeklyCost = (document.getElementById('MoneyInput').value*TimeValue[document.getElementById('MoneyTime').selectedIndex])/52;var WeeklyCO2 = WeeklyCost/BackgroundData[180][2];var CarbonSpend = BackgroundData[182+document.getElementById('MoneyCarbon').selectedIndex][2];var RecyclePaper = parseFloat(BackgroundData[173-document.getElementById('RecyclePaper').selectedIndex][2]);var RecyclePlastic = parseFloat(BackgroundData[176-document.getElementById('RecyclePlastic').selectedIndex][2]);return ((WeeklyCO2+RecyclePaper+RecyclePlastic)*CarbonSpend)*1000;"/*,
    
    "Activism": "return document.getElementById('ActivismInput').value/(parseFloat(BackgroundData[163][2])*(parseFloat(BackgroundData[162][2])-parseFloat(BackgroundData[161][2])));"*/
}

var AreaFunctions = {
    //Stores all the functions in string format so that each calculation can be individual
    "Electricity": "return UserInputs.Electricity*(VariableFigures.electricityIntensity[i])/NumberInHousehold",
    "Gas": "var GasTotal = (UserInputs.Gas-i*NumberInHousehold*((BackgroundData[19][2]-BackgroundData[13][2])/(BackgroundData[20][2]-BackgroundData[10][2])))*0.19/NumberInHousehold;if(GasTotal > 0){return GasTotal}else{return 0}",
    "Oil": "return UserInputs.Oil/NumberInHousehold",
    "Coal": "return UserInputs.Coal/NumberInHousehold",
    "Wood": "return UserInputs.Wood/NumberInHousehold",
    "Water": "return UserInputs.Water/NumberInHousehold",
    
    "Vehicle": "var OneOff = 0; var ElectricCar=(BackgroundData[45][2]/document.getElementById('Mileage').value);if(document.getElementById('FuelType').value=='Electric'){ElectricCar=document.getElementById('Mileage').value*VariableFigures.electricityIntensity[i];}if(document.getElementById('OneOffDateCar').value == VariableFigures.date[i]){OneOff = parseFloat(document.getElementById('OneOffLengthCar').value);}return (UserInputs.Vehicle+OneOff)*ElectricCar",
    "Bus": "var OneOff = 0; if(document.getElementById('OneOffDateBus').value == VariableFigures.date[i]){OneOff = parseFloat(document.getElementById('OneOffLengthBus').value);} var Emissions = BackgroundData[67][2]; if(VariableFigures.date[i] >= BackgroundData[68][2]){Emissions = (BackgroundData[69][2]*VariableFigures.electricityIntensity[i]);}return (UserInputs.Bus+OneOff)*(Emissions)",
    "Train": "var OneOff = 0; if(document.getElementById('OneOffDateTrain').value == VariableFigures.date[i]){OneOff = parseFloat(document.getElementById('OneOffLengthTrain').value);} var Emissions = BackgroundData[72][2]; if(VariableFigures.date[i] >= BackgroundData[73][2]){Emissions = (BackgroundData[74][2]*VariableFigures.electricityIntensity[i]);}return (UserInputs.Train+OneOff)*(Emissions)",
    "Flight": "var OneOff = 0; if(document.getElementById('OneOffDateFlight').value == VariableFigures.date[i]){OneOff = parseFloat(document.getElementById('OneOffLengthFlight').value)*BackgroundData[76][2];}return (UserInputs.Flight+OneOff)",
    
    "Food": "return UserInputs.Food*VariableFigures.foodIntensity[i]",
    "ConsumerGoods": "return UserInputs.ConsumerGoods*VariableFigures.consumerGoodsIntensity[i]",
    "UKShare": "return UserInputs.UKShare*VariableFigures.UKShareIntensity[i]",
    
    "Activism": "var Figure = 1-UserInputs.Activism*i;if(Figure > 0){return Figure}else {return 0}"
}

window.onload = function(){
    //Loads in the CSV file containing all the varibale data and stores in a JSON format
    $.ajax({
      url: 'csv/VariableFigures.csv',
      dataType: 'text',
    }).done(function(data){
        var data = data.split(/\r?\n|\r/);
        for(i=0; i<data.length; i++){
            var Row = data[i].split(",");
            VariableFigures[Row[0]] = Row.slice(1);
        }
    });
    $.ajax({
      url: 'csv/BackgroundData.csv',
      dataType: 'text',
    }).done(function(data){
        var data = data.split(/\r?\n|\r/);
        for(i=0; i<data.length; i++){
            var Row = data[i].split(",");
            BackgroundData.push(Row);
        }
    });
    
    $.ajax({//Loads in the csv containing the solutions 
      url: 'csv/Solutions.csv',
      dataType: 'text',
    }).done(function(data){
        var data = data.split(/\r?\n|\r/);
        for(i=0; i<data.length; i++){
            var Row = data[i].split(",");
            SolutionsAvg[Row[0]] = Row[1]
            Solutions[Row[0]] = Row.slice(2);
        }
    });
}

//Calculates all for working out final figure-For if user doesn't fill all data

function CalculateAll(){
    CalculateFigure("Electricity");
    CalculateFigure("Gas");
    CalculateFigure("Oil");
    CalculateFigure("Coal");
    CalculateFigure("Wood");
    CalculateFigure("Water");
    CalculateFigure("Vehicle");
    CalculateFigure("Bus");
    CalculateFigure("Train");
    CalculateFigure("Flight");
    CalculateFigure("Food");
    CalculateFigure("ConsumerGoods");
    UpdateSection("UKShare");
    //CalculateFigure("Activism");
    FinalCalculation();
}

var NumberInHousehold = 2;
var SectionTotals = new Object();
var SectionTotalsTonne = new Array();
var SectionTotalsFraction = new Array();
var Total = 0;
var TotalTempRise;

var FinalTemps;
var FinalTotal;

function CalculateFigure(Section){
    var Calculation = Function(AreaCalculations[Section]);
    UserInputs[Section] = Calculation();
    UpdateSection(Section);
}

function UpdateSection(Section){
    //calculates the energy usage for each year
    var SectionTotal = 0;
    for(i=0; i<VariableFigures.date.length; i++){//Gets number of years from json file
        var Calculation = Function(AreaFunctions[Section]);
        SectionTotal = SectionTotal + Calculation();
        //console.log(i + " " + Calculation());
    }
    SectionTotals[Section] = SectionTotal;
    //FinalCalculation();//Final Calculation for dynamic updates-Not this version
}

function FinalCalculation(){
    console.log(SectionTotals);
    //The Final calculation for working out the final score.
    SectionTotalsTonne = new Array(Object.keys(UserInputs).length);
    SectionTotalsFraction = new Array(Object.keys(UserInputs).length);
    Total = 0;
    
    //Calculates each elements tonnage
    for(i=0; i<SectionTotalsTonne.length; i++){
        SectionTotalsTonne[i] = SectionTotals[(Object.keys(UserInputs)[i])]/1000;
        Total = Total + SectionTotalsTonne[i];
    }
    console.log(Total);
    //Calculates the percentage of CO2 caused by each area
    for(i=0; i<SectionTotalsTonne.length; i++){
        SectionTotalsFraction[i] = SectionTotalsTonne[i]/Total;
    }
    //Calculates the total
    if(Total<=BackgroundData[205][2]){
        var BudgetCalc =  (2.0-1.5)/(BackgroundData[205][2]-BackgroundData[204][2]);
        TotalTempRise = 1.5 + (Total-BackgroundData[204][2])*BudgetCalc;
    } else {
        var BudgetCalc =  (3.0-2.0)/(BackgroundData[206][2]-BackgroundData[205][2]);
        TotalTempRise = 2 + (Total-BackgroundData[205][2])*BudgetCalc;
    }
    console.log(TotalTempRise);
    //Works out the final total
    TotalTempRise = TotalTempRise - (1.5-BackgroundData[204][2]*(2.0-1.5)/(BackgroundData[205][2]-BackgroundData[204][2]));
    console.log(TotalTempRise);
    FinalTemps = new Array(SectionTotalsTonne.length);
    for(i=0; i<SectionTotalsTonne.length; i++){
        FinalTemps[i] = SectionTotalsFraction[i]*TotalTempRise;
        console.log(SectionTotalsFraction[i] + "*" + TotalTempRise + "=" + FinalTemps[i]);
    }
    FinalTotal = FinalTemps.reduce(getSum);
    FinalTotal+= 1.33;
    console.log(FinalTemps);
    console.log("Final Total: " + FinalTotal);
    console.log("TotalTempRise: " + TotalTempRise);
    setResult();
}

//Works out the sum of all numbers in an array
function getSum(Total, Num){
    return Total + Num;
}

//Sets the results of the calculation and display them
function setResult(){
    var ListNum = 0;
    var Result = "<p class='FinalScore'>Your final score is: " + FinalTotal.toFixed(2) + "&deg;C</p><p class='FinalScore'>Here are a few suggestions for ways to improve this:</p><ul id='Suggestions'>";
    for(i=0; i<FinalTemps.length-1; i++){
        if(FinalTemps[i] > SolutionsAvg[Object.keys(UserInputs)[i]] && ListNum < 7){
            Result = Result + "<li class='FinalScore' style='display: list-item'>" + Solutions[Object.keys(UserInputs)[i]][parseInt(Math.random()*Solutions[Object.keys(UserInputs)[i]].length)] + "</li>";
            ListNum++;
        }
    }
    Result = Result + "</ul><div id='socialMedia' style='list-style-type:disc'><a href='javascript:FacebookShare()' class='fa fa-facebook'></a><a href='javascript:TwitterShare()' class='fa fa-twitter'></a><a href='javascript:EmailShare()' class='fa fa-envelope'></a></div><div id='emailSection'><p>Please enter your email address below</p><input id='UserEmail' type='email' placeholder='Email Address' value=''/><input type='button' value='Send' onclick='SendEmail()'/><br><br><input type='button' onclick='document.getElementById(\"socialMedia\").style.display = \"block\";document.getElementById(\"emailSection\").style.display = \"none\";' value='&lt;Back'/></div>";
    for(i=0; i<10; i++){
        document.getElementsByClassName("TreeImage")[i].style.left = "-" + document.getElementsByClassName("TreeImage")[i].offsetWidth + "px";
    }
    document.getElementById("TextBackgroundParent").style.top = "-" + window.innerHeight + "px";
    document.getElementById("newsFeed").style.top = "-" + window.innerHeight + "px";
    setTimeout(function(){
        document.getElementById("Results").innerHTML = Result;
        document.getElementById("Results").style.transform = "rotateX(0deg)";
        document.getElementById("disclaimer").style.bottom = "0px";
    }, 1000)
}